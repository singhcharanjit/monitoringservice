﻿using System;
using System.Collections.Generic;
using System.Text;
using PRISM.Observer.StateMachine.Classes;

namespace PRISM.Observer.Classes
{
    public class Endpoint
    {
        private EndpointStateMachine _sm;
        public Endpoint()
        {
            _sm = new EndpointStateMachine(this);
        }

        #region State Machine Methods

        public void smInitEndpoint() { }
        public void smTransitionDone() { }
        public void smSendEndpointRequest() { }
        public void smParsePayload() { }
        public void smHandleHttpError() { }
        public void smHandlePayloadErrorFatal() { }
        public void smHandlePartialPayloadError() { }
        public void smHandleLoadComplete() { }
        public void smSavingEndpointState() { }
        public void smUnloadEndpoint() { }
        public void smLoadingUnderlyingServicesFromDB() { }
        public void smHandleUnderlyingServiceNotFound() { }
        public void smPerpareUnderlyingServices() { }
        public void smSortUnderlyingServices() { }
        public void smLoadUnderlyingServiceDetails() { }
        public void smGetNextUnderlyingService() { }
        public void smExecuteUnderlyingService() { }

        #endregion
    }
}
