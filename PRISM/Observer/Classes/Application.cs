﻿using System;
using System.Collections.Generic;
using System.Text;
using PRISM.Observer.StateMachine.Classes;

namespace PRISM.Observer.Classes
{
    public class Application
    {
        private ApplicationStateMachine _sm;

        public Application()
        {
            _sm = new ApplicationStateMachine(this);
        }

        #region State Machine Methods
        public void smInitApp() { }
        public void smTransitionDone() { }
        public void smFetchAllRequestDetailsFromDB() { }
        public void smPrepareEndpoints() { }
        public void smSortEndpoints() { }
        public void smGetNextEndpoint() { }
        public void smSaveApplicationState() { }
        public void smUnloadApplication() { }
        public void smExecuteEndpoint() { }
        #endregion

    }
}
