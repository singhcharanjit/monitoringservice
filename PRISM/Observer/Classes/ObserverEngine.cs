﻿
using PRISM.Observer.StateMachine.Classes;

namespace PRISM.Observer.Classes
{
    public class ObserverEngine
    {
        private ObserverEngineStateMachine _sm;
        public ObserverEngine()
        {
            _sm = new ObserverEngineStateMachine(this);
        }


        #region State Machine Methods
        public void smInitObserver(){}
        public void smSwitchToDormantState() { }
        public void smSuspendTimer() { }
        public void smTransitionDone() { _sm.Done(); }
        public void smPrepareSession() { }
        public void smGetAuthDetailsFromDB() { }
        public void smAuthenticate() { }
        public void smAnalyzeAuthResponse() { }
        public void smLoadApplicationsFromDB() { }
        public void smPrepareApplications() { }
        public void smSortApplications() { }
        public void smGetNextApplication() { }
        public void smExecuteApplication() { }
        public void smHandleAppLoadingError() { }
        public void smHandleInvalidAuthResponse() { }
        public void smHandleLoginFailure() { }
        public void smHandleSessionCreationFailure() { }
        public void smSavingFinalState() { }
        public void smPrepareNotification() { }
        public void smSendNotification() { }
        public void smReleaseResources() { }
        #endregion
    }
}
