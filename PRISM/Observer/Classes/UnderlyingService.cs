﻿using System;
using System.Collections.Generic;
using System.Text;
using PRISM.Observer.StateMachine.Classes;

namespace PRISM.Observer.Classes
{
    public class UnderlyingService
    {
        private UnderlyingServiceStateMachine _sm;
        public UnderlyingService()
        {
            _sm = new UnderlyingServiceStateMachine(this);
        }

        #region State Machine Methods
        public void smInitUnderlyingService() { }
        public void smTransitionDone() { }
        public void smSendUnderlyingServiceRequest() { }
        public void smHandleHttpError() { }
        public void smParsePayload() { }
        public void smSavingUnderlyingServiceState() { }
        public void smUnloadUnderlyingService() { }
        public void smHandlePayloadErrorFatal() { }
        public void smHandlePartialPayloadError() { }
        public void smHandleLoadComplete() { }

        #endregion
    }
}
