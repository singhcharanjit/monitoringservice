@echo off
echo Generating State Machines

java -jar ..\ExternalDependencies\smc\Smc.jar -csharp -g -d .\statemachine\classes\ .\statemachine\ObserverEngineStateMachine.sm
java -jar ..\ExternalDependencies\smc\Smc.jar -csharp -g -d .\statemachine\classes\ .\statemachine\ApplicationStateMachine.sm
java -jar ..\ExternalDependencies\smc\Smc.jar -csharp -g -d .\statemachine\classes\ .\statemachine\EndpointStateMachine.sm
java -jar ..\ExternalDependencies\smc\Smc.jar -csharp -g -d .\statemachine\classes\ .\statemachine\UnderlyingServiceStateMachine.sm


echo Completed...
